package com.jrsokolow.e3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
// Trying to deploy app on JBoss
// @Configuration
//@ComponentScan
//@EnableAutoConfiguration
//extends SpringBootServletInitializer
public class E3Application {

	public static void main(String[] args) {
		SpringApplication.run(E3Application.class, args);
	}

// Trying to deploy app on JBoss
//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		return application.sources(applicationClass);
//	}
//
//	private static Class<E3Application> applicationClass = E3Application.class;
}
