package com.jrsokolow.e3.controller;

import com.jrsokolow.e3.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class UserController {

    @GetMapping("/user")
    public String displayForm(Model model, HttpSession session) {
        User user = new User();
        if(session.getAttribute("user") != null) {
            user = (User) session.getAttribute("user");
        }
        model.addAttribute("user", user);
        return "user";
    }

    @PostMapping("/user")
    public String submitForm(@Valid User user, BindingResult bindingResult, HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "user";
        }

        session.setAttribute("user", user);

        return "result";
    }

}
